package com.example.guneetsingh_mad5244_c0727012_test1andtest2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Finger {

    int xPosition;
    int yPosition;
    int direction = -1;
    Bitmap fingerImage;
    final int speed=50;
    private Rect hitBox;
    public Finger(Context context, int x, int y) {
        this.fingerImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.finger01);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.fingerImage.getWidth(), this.yPosition + this.fingerImage.getHeight());

    }

    public void updatefingerPosition() {
        if (this.direction <= 0) {
            // move left
            this.xPosition = this.xPosition - speed;
        }
        else if (this.direction == 1) {
            // move righ
            this.xPosition = this.xPosition + speed;
        }
       else if (this.direction == 2) {
            // move up
            this.yPosition = this.yPosition - speed;
        }
        else if (this.direction == 3) {
            // move up
            this.yPosition = 600;
        }



        // update the position of the hitbox
        this.updateHitbox();
    }
    public void updateHitbox() {
        // update the position of the hitbox
        this.hitBox.top = this.yPosition;
        this.hitBox.left = this.xPosition;
        this.hitBox.right = this.xPosition + this.fingerImage.getWidth();
        this.hitBox.bottom = this.yPosition + this.fingerImage.getHeight();
    }

    public Rect getHitbox() {

        return this.hitBox;
    }

    public void setXPosition(int x) {
        this.xPosition = x;
        this.updateHitbox();
    }
    public void setYPosition(int y) {
        this.yPosition = y;
        this.updateHitbox();
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    /**
     * Sets the direction of the player
     * @param i     0 = down, 1 = up
     */
    public void setDirection(int i) {
        this.direction = i;
    }
    public Bitmap getBitmap() {
        return this.fingerImage;
    }



}
