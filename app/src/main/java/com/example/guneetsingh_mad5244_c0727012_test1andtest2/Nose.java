package com.example.guneetsingh_mad5244_c0727012_test1andtest2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Nose {
    int xPosition;
    int yPosition;
    int direction = -1;
    Bitmap noseImage;
    private Rect hitBox;
    private Rect hitBox2;

    public Nose(Context context, int x, int y) {
        this.noseImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.nose01);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.noseImage.getWidth(), this.yPosition + this.noseImage.getHeight());
        this.hitBox2 = new Rect(this.xPosition, this.yPosition, this.xPosition + this.noseImage.getWidth(), this.yPosition + this.noseImage.getHeight());
    }

    public void updatenosePosition() {
//        if (this.direction <= 0) {
//            // move right
//            this.xPosition = this.xPosition - 30;
//        }
//        else if (this.direction == 1) {
//            // move left
//            this.xPosition = this.xPosition + 30;
//        }

        // update the position of the hitbox
        this.updateHitbox();
        this.updateHitbox2();
    }
    public void updateHitbox() {
        // update the position of the hitbox
        this.hitBox.top = this.yPosition+this.noseImage.getHeight()/2+100;
        this.hitBox.left = this.xPosition+100;
        this.hitBox.right = this.xPosition + this.noseImage.getWidth()-350;
        this.hitBox.bottom = this.yPosition + this.noseImage.getHeight()-50;
    }
    public void updateHitbox2() {
        this.hitBox2.top = this.yPosition+this.noseImage.getHeight()/2+100;
        this.hitBox2.left = this.hitBox.right+100;
        this.hitBox2.right = this.xPosition + this.noseImage.getWidth()-100;
        this.hitBox2.bottom = this.yPosition + this.noseImage.getHeight()-50;
    }

    public Rect getHitbox() {

        return this.hitBox;
    }
    public Rect getHitBox2(){
        return  this.hitBox2;
    }

    public void setXPosition(int x) {
        this.xPosition = x;
        this.updateHitbox();
        this.updateHitbox2();
    }
    public void setYPosition(int y) {
        this.yPosition = y;
        this.updateHitbox();
        this.updateHitbox2();
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    /**
     * Sets the direction of the player
     * @param i     0 = down, 1 = up
     */
    public void setDirection(int i) {
        this.direction = i;
    }
    public Bitmap getBitmap() {
        return this.noseImage;
    }
}
