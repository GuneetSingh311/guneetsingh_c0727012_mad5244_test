package com.example.guneetsingh_mad5244_c0727012_test1andtest2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="NOSE-GAME";

    // screen size
    int screenHeight;
    int screenWidth;
    String status = "Playing";

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------
    Nose nose;
    Finger finger;

    // ----------------------------
    // ## GAME STATS
    // ----------------------------
    int score = 0;
    int lives = 5;

    public GameEngine(Context context, int w, int h) {
        super(context);


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();

        // @TODO: Add your sprites
        this.spawnFinger();
        this.spawnNose();
      //  this.spawnEnemyShips();
        // @TODO: Any other game setup
    }



    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }



    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }
    private  void spawnNose(){
        nose = new Nose(this.getContext(), (this.screenWidth-300) / 2, 40);
    }
    private void spawnFinger() {
        // put player in middle of screen --> you may have to adjust the Y position
        // depending on your device / emulator
        finger = new Finger(this.getContext(), (this.screenWidth-300) / 2, 600);

    }


    public void updatePositions() {
        nose.updatenosePosition();
        finger.updatefingerPosition();

        if (finger.getXPosition() <= 0) {

            //reset the enemy's starting position to right side of screen
            // you may need to adjust this number according to your device/emulator
         finger.setDirection(1);
        }
        else if(finger.getXPosition() >= screenWidth-20) {
            int x = finger.xPosition - 15;
           finger.setDirection(-1);
        }




        // @TODO: Collision detection between nose and finger
      if (finger.getHitbox().intersect(nose.getHitbox())) {

            // reset player to original position
            this.status = "YOUWIN";
          this.gameIsRunning = false;

        }
        else if(finger.getHitbox().intersect(nose.getHitBox2())){

          this.status = "YOUWIN";
          this.gameIsRunning = false;
        }
      
        else if(finger.getYPosition() < 0){
          this.status = "YOU LOSE";
          finger.setDirection(3);
      }
    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.RED);


            //@TODO: Draw the nose
            canvas.drawBitmap(this.nose.getBitmap(), this.nose.getXPosition(), this.nose.getYPosition(), paintbrush);

            //@TODO: Draw the finger
            canvas.drawBitmap(this.finger.getBitmap(), this.finger.getXPosition(), this.finger.getYPosition(), paintbrush);


            // Show the hitboxes on player and enemy
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(5);
            Rect fingerHitbox = finger.getHitbox();
            canvas.drawRect(fingerHitbox.left, fingerHitbox.top, fingerHitbox.right, fingerHitbox.bottom, paintbrush);

            Rect noseHitboxHitbox = nose.getHitbox();
            Rect noseHitbox2 = nose.getHitBox2();
            canvas.drawRect(noseHitboxHitbox.left, noseHitboxHitbox.top, noseHitboxHitbox.right, noseHitboxHitbox.bottom, paintbrush);
            canvas.drawRect(noseHitbox2.left, noseHitbox2.top, noseHitbox2.right, noseHitbox2.bottom, paintbrush);
            canvas.drawText(this.status, 100, 400, paintbrush);
            // draw game stats
            paintbrush.setTextSize(60);
            paintbrush.setColor(Color.BLACK);


            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // move player up
            this.finger.setDirection(2);

        }

        return true;
    }
}
